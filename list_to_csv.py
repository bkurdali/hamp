#!/usr/bin/python3

"""
Convert an html table into a .csv file
"""

import os
import csv
import sys


def get_lists(text):
    """ assumes no nested lists """
    lists = []
    chunks = [chunk for chunk in text.split('<ul>')][1:]
    for chunk in chunks:
        items = chunk.split('</ul>')[0]
        lists.append([item.replace('</li>','') for item in items.split('<li>')])
    return lists


def format_rows(rows):
    split_rows = []
    for row in rows:
        if not row.strip():
            continue
        items = row.strip().split(": <a href=")
        try:
            number = int(items[0].strip().split()[-1])
        except ValueError:
            number = ""
        number = str(number)
        if number:
            header = items[0].replace(number,"").strip().title()
        else:
            header = items[0].strip().title()
        link = f'<a href="{items[-1]}' if '</a>' in items[-1] else items[-1]
        split_rows.append((header, number, link))
    # print(split_rows)
    return split_rows

def first(rows):
    return list({row[0] for row in rows})

def empty(row):
    return ['' for i in row]

def get_index(first_row, row):
    return first_row.index(row[0])

def build_table_sparse(rows):
    first_row = first(rows)
    table = [first_row, ]
    for row in rows:
        new_row = empty(first_row)
        new_row[get_index(first_row,row)] = row[-1]
        table.append(new_row)
    print(table)
    return table


def build_table(rows):
    first_row = first(rows)
    new_rows = [first_row, empty(first_row),]
    for row in rows:
        index = get_index(first_row, row)
        for new_row in new_rows[1:]:
            if not new_row[index]:
                new_row[index] = row[-1]
                break
        else:
            new_rows.append(empty(first_row))
            new_rows[-1][index] = row[-1]
    print(new_rows)
    return new_rows



def make_csvs(name, tables):
    for idx, table in enumerate(tables):
        suffix = f'_{idx + 1}' if idx else ''
        with open(f"{name}{suffix}.csv", "w") as output_file:
            csv_writer = csv.writer(output_file)
            for row in table:
                csv_writer.writerow(row)

if __name__ == "__main__":
    args = sys.argv
    if len(args) < 2:
        print("Usage: list_to_csv.py filename")
    else:
        blob = open(args[-1]).read()
        name = args[-1].split('.')[0]
        raw_lists = get_lists(blob)
        make_csvs(
            name,
            [build_table(format_rows(raw)) for raw in get_lists(blob)]
                  )   
