#!/usr/bin/python3

import csv
import sys
import os

standalone_table = False
overwrite_html = True

class TableMaker:
    header = '''      <section aria-label="{}" id="{}">
        <h2>{}</h2>
        <figure>
          <table>'''
    head_start = '            <thead>'
    head_end = '            </thead>'
    body_start = '            <tbody>'
    body_end = '            </tbody>'
    row_start = '              <tr>'
    row_end = '              </tr>'
    col_header = '                <th scope="col">{}</th>'
    row_header = '                <th scope="row">{}</th>'
    row = '                <td>{}</td>'
    footer = '''          </table>
        </figure>
      </section>'''

    def __init__(self, section_name):
        self.header = self.header.format(
            section_name,
            section_name.split()[-1].lower(),
            section_name)
        self.lines = self.header.split('\n')
        self.lines.append(self.head_start)
        self.built_header = False
        self.done = False

    def add(self, row):
        if not self.built_header:
            first = "{}".format(self.col_header)
            other = "{}".format(self.col_header)
        else:
            first = "{}".format(self.row_header)
            other = "{}".format(self.row)

        self.lines.append(self.row_start)
        for i, entry in enumerate(row):
            if i == 0:
                self.lines.append(first.format(entry))
            else:
                self.lines.append(other.format(entry))
        self.lines.append(self.row_end)
        if not self.built_header:
            self.lines.append(self.head_end)
            self.lines.append(self.body_start)
            self.built_header = True

    def output(self):
        if not self.done:
            if self.built_header:
                self.lines.append(self.body_end)
            self.lines.extend(self.footer.split('\n'))
            self.done = True
        return '\n'.join(self.lines)


class Merger:

    def __init__(self, row):
        """ Takes a list of two-ples """
        pairs = [
            (i - 1, row[i - 1]) for i, v in enumerate(row)
            if v.lower().startswith('link')]

        self.columns = []
        while pairs:
            pops = [pairs[0]] + [
                p for p in pairs[1:] if p[1].startswith(pairs[0][1])]
            self.columns.append([p[0] for p in pops])
            for p in pops:
                pairs.remove(p)
        self.processed_header = False

    def row(self, row):
        """ process a row """
        if not self.processed_header:
            self.pops = []
            self.links = [c[0] for c in self.columns]
            for columns in self.columns:
                self.pops.append(columns[0] + 1)
                for column in columns[1:]:
                    self.pops.extend([column, column + 1])
            new_row = [val for idx, val in enumerate(row) if idx not in self.pops]
            self.processed_header = True
            while new_row[-1] == "":
                del(new_row[-1])
            self.length = len(new_row)
            return new_row
        # build the links
        links = {}
        for columns in self.columns:
            link = []
            for column in columns:
                if row[column] and row[column + 1]:
                    link.append(f'<a href="{row[column + 1]}">{row[column]}</a>')
                elif row[column]:
                    link.append(row[column])
                elif row[column + 1]:
                    link.append(f'<a href="{row[column + 1]}">link</a>')
            link = ','.join(link)
            links[columns[0]] = link
        # build the new row
        new_row = []
        for idx, item in enumerate(row):
            if idx in self.links:
                new_row.append(links[idx])
            elif idx not in self.pops:
                new_row.append(item)
        while new_row[-1] == "" and len(new_row) > self.length:
            del(new_row[-1])
        return new_row

if __name__ == "__main__":
    args = sys.argv
    if len(args) > 1 and args[1].endswith('.csv') and os.path.exists(args[1]):
        with open(args[1], newline='') as csv_file:
            html_writer = TableMaker(os.path.split(args[1][:-4])[-1])
            csv_reader = csv.reader(csv_file)
            link_merger = None
            for row in csv_reader:
                if link_merger is None: # first read
                    link_merger = Merger(row)
                html_writer.add(link_merger.row(row))
        if standalone_table or not args[-1].endswith('.html'):
            html_name = args[1].replace('.csv', '.html')
            with open(html_name, 'w') as html_file:
                html_file.write(html_writer.output())
        if len(args) > 2 and args[-1].endswith('.html') and os.path.exists(args[-1]):
            with open(args[-1]) as web_page:
                old_html = web_page.read()
            new_html = '\n'.join([
                old_html.split(html_writer.header)[0],
                html_writer.output(),
                old_html.split(TableMaker.footer)[-1]])
            new_name = args[-1] if overwrite_html else args[-1].replace('.html', '_new.html')
            with open(new_name, 'w') as html_file:
                html_file.write(new_html)
    else:
        print("Usage: csv_to_table.py original_data.csv [htmltoinsert.html]")


        
