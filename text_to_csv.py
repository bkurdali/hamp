#!/usr/bin/python3

import csv
import sys
import os


def make_csv(input_path):
    with open(f"{input_path}.csv", "w") as output_file:
        csv_writer = csv.writer(output_file)
        with open(args[1]) as input_file:
            for line in input_file:
                if line.strip():
                    csv_writer.writerow([
                        f'"{c.strip().replace('Nada','')}"' if ',' in c else
                        c.strip().replace('Nada','')
                        for c in line.split("  ") if c.strip()])

if __name__ == "__main__":
    args = sys.argv
    if len(args) != 2 or not os.path.isfile(sys.argv[1]):
        print("Usage: text_to_csv.py input_text")
    else:
        make_csv(sys.argv[1])
